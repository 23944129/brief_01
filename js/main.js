// Main javascript file

// Scroll animation to move lid parts once the screen reaches the #movinglid section.
// The start position, ending after 

gsap.timeline({
    scrollTrigger: {
      trigger: "#movingLid", // section id triggers animation.
      start: "top center",  // start the animation when the top of the trigger hits the center of the viewport.
      end: "+=100",   // finish after scrolling 100px past the start
      scrub:1,    // smooth scrubbing takes 1 second to catch up with the scrollbar.
     
    }
  })
  // Moves images of the lid parts from these positions into place
    .from("#dateRing", {yPercent: 130}, 0)
    .from("#disc",  {yPercent: -140, xPercent: 120}, 0)
    .from("#filter", {yPercent:-190, xPercent: -160 }, 0)
  
    